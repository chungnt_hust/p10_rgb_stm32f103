/*
 * font.h
 *
 *  Created on: Jan 29, 2019
 *      Author: chungnguyen
 */

#ifndef FONT_H_
#define FONT_H_
#include "stm32f1xx_hal.h"

typedef struct
{
	uint8_t code[8][5];
} font58_t;

const font58_t font58[] =
{
	// số 0
	0,  0,  0,  0,  0,
	0,	1,	1,	1,	0,
	1,	0,	0,	0,	1,
	1,	0,	0,	0,	1,
	1,	0,	0,	0,	1,
	1,	0,	0,	0,	1,
	1,	0,	0,	0,	1,
	0,	1,	1,	1,	0,

	// số 1
	0,	0,	0,	0,	0,
	0,	0,	1,	0,	0,
	0,	1,	1,	0,	0,
	0,	0,	1,	0,	0,
	0,	0,	1,	0,	0,
	0,	0,	1,	0,	0,
	0,	0,	1,	0,	0,
	0,	1,	1,	1,	0,

	// số 2
	0,	0,	0,	0,	0,
	0,	1,	1,	1,	0,
	1,	0,	0,	0,	1,
	1,	0,	0,	0,	1,
	0,	0,	0,	1,	0,
	0,	0,	1,	0,	0,
	0,	1,	0,	0,	0,
	1,	1,	1,	1,	1,

	// số 3
	0,	0,	0,	0,	0,
	0,	1,	1,	1,	0,
	1,	0,	0,	0,	1,
	0,	0,	0,	0,	1,
	0,	0,	0,	1,	0,
	0,	0,	0,	0,	1,
	1,	0,	0,	0,	1,
	0,	1,	1,	1,	0,

};


#endif /* FONT_H_ */
