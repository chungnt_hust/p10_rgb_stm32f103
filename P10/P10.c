/*
 * P10.c
 *
 *  Created on: Jan 24, 2019
 *      Author: chungnguyen
 */
#include "P10.h"
#include "font.h"
#include <stdbool.h>
/*
 *  HCLK = 72MHz
 	SysTick->LOAD = 72000;
	SysTick->VAL  = 0;
	<=> 1ms
*/
#define TIME_INT (500) // 7us
GPIO_TypeDef *gpioPort[] = {GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB};
uint32_t valueGpioSet[]   = {GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2, GPIO_PIN_3, GPIO_PIN_4, GPIO_PIN_5, GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2, GPIO_PIN_10, GPIO_PIN_11, GPIO_PIN_12};
uint32_t valueGpioReSet[] = {GPIO_PIN_0<<16, GPIO_PIN_1<<16, GPIO_PIN_2<<16, GPIO_PIN_3<<16, GPIO_PIN_4<<16, GPIO_PIN_5<<16, GPIO_PIN_0<<16, GPIO_PIN_1<<16, GPIO_PIN_2<<16, GPIO_PIN_10<<16, GPIO_PIN_11<<16, GPIO_PIN_12<<16};
GPIO_TypeDef *gpioDataPort = GPIOA;
pixel_t Panel[MAX_ROW][MAX_COL]; // dữ liệu = 1 <=> led sáng
uint32_t timeSlice[MAX_BIT_POS] = {1*TIME_INT-1, 2*TIME_INT-1, 4*TIME_INT-1, 8*TIME_INT-1, 16*TIME_INT-1, 32*TIME_INT-1, 64*TIME_INT-1, 128*TIME_INT-1};
point_2D_t P10;
bool enDpl;

Encode_t DataOutput[32];

static inline void switchRowP10(uint8_t row)
{
	(row & 0x1) ? (gpioPort[A_PIN]->BSRR = (uint32_t)valueGpioSet[A_PIN]):(gpioPort[A_PIN]->BSRR = (uint32_t)valueGpioReSet[A_PIN]);
	(row & 0x2) ? (gpioPort[B_PIN]->BSRR = (uint32_t)valueGpioSet[B_PIN]):(gpioPort[B_PIN]->BSRR = (uint32_t)valueGpioReSet[B_PIN]);
	(row & 0x4) ? (gpioPort[C_PIN]->BSRR = (uint32_t)valueGpioSet[C_PIN]):(gpioPort[C_PIN]->BSRR = (uint32_t)valueGpioReSet[C_PIN]);
}

static inline void enablePanel(void)
{
	gpioPort[OE_PIN]->BSRR = valueGpioReSet[OE_PIN];
}

static inline void disablePanel(void)
{
	gpioPort[OE_PIN]->BSRR = valueGpioSet[OE_PIN];
}

static inline void shiftData(uint8_t row, uint8_t pos)
{
	uint8_t x;
	for(x = 0; x < MAX_COL; x++)
	{
		gpioDataPort->ODR = ((uint32_t)DataOutput[x].encodeData[row][pos]);
		gpioPort[CLK_PIN]->BSRR = valueGpioSet[CLK_PIN];
		gpioPort[CLK_PIN]->BSRR = valueGpioReSet[CLK_PIN];
	}
}

static inline void latchData(void)
{
	gpioPort[LAT_PIN]->BSRR = (uint32_t)valueGpioSet[LAT_PIN];
	gpioPort[LAT_PIN]->BSRR = (uint32_t)valueGpioReSet[LAT_PIN];
}

void HAL_SYSTICK_Callback(void)
{
	P10_IRQHandle();
}

inline void P10_IRQHandle(void)
{
	static uint8_t row;
	static uint8_t bitPos;
	disablePanel();
	shiftData(row, bitPos);
	latchData();
	switchRowP10(row);
	enablePanel();

	SysTick->LOAD = timeSlice[bitPos];
	SysTick->VAL  = 0;

	bitPos++;
	if(bitPos >= MAX_BIT_POS)
	{
		bitPos = 0;
		row++;
		if(row >= MAX_ROW_SCAN) row = 0;
	}
}

/* 0 <= row <= 15; 0 <= col <= 31 */
static void P10_position(uint8_t row, uint8_t col)
{
	P10.x = col;
	P10.y = row;
}

/* 0 <= r, g, b <= 255*/
static void P10_color(uint8_t r, uint8_t g, uint8_t b)
{
	Panel[P10.y][P10.x].r = r;
	Panel[P10.y][P10.x].g = g;
	Panel[P10.y][P10.x].b = b;
}

void P10_setColorAt(uint8_t row, uint8_t col, uint8_t r, uint8_t g, uint8_t b)
{
	P10_position(row, col);
	P10_color(r, g, b);
}

void P10_display(void)
{
	if(enDpl)
	{
		uint8_t row, col, pos;
		for(col = 0; col < MAX_COL; col++)
		{
			for(row = 0; row < MAX_ROW_SCAN; row++)
			{
				uint8_t temRow = row+8;
				for(pos = 0; pos < MAX_BIT_POS; pos++)
				{
					DataOutput[col].encodeData[row][pos] = ((Panel[row][col].r>>pos & 0x1)<<0) |
														   ((Panel[row][col].g>>pos & 0x1)<<1) |
														   ((Panel[row][col].b>>pos & 0x1)<<2) |
														   ((Panel[temRow][col].r>>pos & 0x1)<<3) |
														   ((Panel[temRow][col].g>>pos & 0x1)<<4) |
														   ((Panel[temRow][col].b>>pos & 0x1)<<5);
				}
			}
		}
		enDpl = false;
	}
}

void P10_enableDisplay(void)
{
	enDpl = true;
}

void P10_init(void)
{
	for(uint8_t i = 0; i < 32; i++) { if(i%4 < 3) { P10_setColorAt(0, i, 0, 0, 20); P10_setColorAt(15, i, 0, 0, 20); } }
	for(uint8_t i = 1; i < 15; i++) { if(i%4 < 3) { P10_setColorAt(i, 0, 0, 0, 20); P10_setColorAt(i, 31, 0, 0, 20); } }

	for(uint8_t row = 0; row < 8; row++)
	{
	  for(uint8_t col = 0; col < 5; col++)
	  {
		  P10_setColorAt(row, col+1, 0, font58[3].code[row][col]*200, 0);
	  }
	}
}
